/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                          {'O', '-', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', '-', 'O'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                          {'X', '-', '-'}, 
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                          {'-', 'X', '-'}, 
                          {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', '-', 'X'}, 
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'O', 'O', 'O'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'}, 
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'X', 'X', 'X'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                          {'-', '-', '-'}, 
                          {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckX1PlayerOWin() {
        char table[][] = {{'O', '-', '-'}, 
                          {'-', 'O', '-'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerOWin() {
        char table[][] = {{'-', '-', 'O'}, 
                          {'-', 'O', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckX1PlayerXWin() {
        char table[][] = {{'X', '-', '-'}, 
                          {'-', 'X', '-'}, 
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true,OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXWin() {
        char table[][] = {{'-', '-', 'X'}, 
                          {'-', 'X', '-'}, 
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true,OXProgram.checkX2(table, currentPlayer));
    }
}
